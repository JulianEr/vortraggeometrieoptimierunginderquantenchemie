import math
import py3Dmol
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib.animation as animation

from IPython.display import HTML
import numpy as np

with open("water.xyz") as ifile:
    water = ifile.read()
with open("cetirizine.xyz") as ifile:
    cetirizin = ifile.read()
with open("2water.xyz") as ifile:
    two_waters = ifile.read()

def numeric_first_derivative(f, x, eps=1e-8):
    return (f(x+eps)-f(x-eps))/(2.0*eps)

def numeric_secon_derivative(f, x, eps=1e-4):
    return (f(x+2.0*eps) + f(x-2.0*eps) - 2.0*f(x))/(4.0*eps*eps)
    #return (f(x+2.0*eps) - 2.0*f(x+eps) + f(x))/(eps*eps)

def newton_step_return_value_and_gradient(f, x, eps=1e-8):
    E, g = f(x), numeric_first_derivative(f, x, eps)
    return -(g/numeric_secon_derivative(f, x, math.sqrt(eps))), E, g

def quadratic_approximation_at(f, x):
    E, g, H = f(x), numeric_first_derivative(f, x), numeric_secon_derivative(f, x)
    return lambda l: E + g*(l-x) + 0.5*H*(l-x)**2

def optimize(f, x, g_threshold=1e-6):
    xs = [x]
    qs = [quadratic_approximation_at(f, x)]
    d,E,g = newton_step_return_value_and_gradient(f, x)
    ys = [E]
    for i in range(10):
        x += d
        xs.append(x)
        qs.append(quadratic_approximation_at(f, x))
        E_old = E
        d, E, g = newton_step_return_value_and_gradient(f, x)
        ys.append(E)
        if abs(g) < g_threshold:
            return xs, ys, qs
        
    print("Not converged after 10 steps!")

class NewtonAnimation(object):
    def __init__(self, f, ax, lims=(-1.0, 5.0), quadratic_with=2.0):
        x_line = np.arange(lims[0], lims[1], 0.1)
        self.main_line = ax.plot(x_line, [f(x) for x in x_line], lw=2)
        self.line_quadratic, = ax.plot([], [], 'g-')
        self.line_scattered, = ax.plot([], [], 'ro')
        self.ax = ax
        self.quadratic_with = quadratic_with
        
    def get_main_line(self):
        return self.main_line, self.line_quadratic, self.line_scattered
    
    def __call__(self, xq):
        x_, q = xq
        xs = np.arange(x_-self.quadratic_with, x_+self.quadratic_with+0.1, 0.05)
        self.line_quadratic.set_data(xs, [q(x) for x in xs])
        self.line_scattered.set_data([x_], [q(x_)])
        return self.main_line, self.line_quadratic, self.line_scattered
    
def make_and_return_animation(fig, xs, qs, newtonAnimation):
    return animation.FuncAnimation(fig, newtonAnimation, frames=zip(xs, qs), init_func=lambda: (newtonAnimation.get_main_line(),), interval=500, repeat=False, save_count=len(xs))

def draw_water_molecule(style, width=300, height=300):
    #view_water = py3Dmol.view(width=width, height=height)
    pass
    # view_water.addModelsAsFrames(water)
    # view_water.setStyle({'model': -1}, {style: {'color': 'spectrum'}})
    # view_water.zoomTo()
    # view_water.show()

def draw_cetirizin_molecule(style, width=400, height=400):
    pass
    # view_cetirizin = py3Dmol.view(width=width, height=height)
    # view_cetirizin.addModelsAsFrames(cetirizin)
    # view_cetirizin.setStyle({'model': -1}, {style: {'color': 'spectrum'}})
    #view_cetirizin.zoomTo()
    #view_cetirizin.show()

def draw_1hvr_molecule(style, width=400, height=400):
    view_1hvr = py3Dmol.view(query='pdb:1hvr', width=width, height=height)
    view_1hvr.setStyle({style:{'color':'spectrum'}})
    view_1hvr.show()

def draw_both_water_molecules(style, width=400, height=400):
    pass
    # view_2water = py3Dmol.view(width=width, height=height)
    # view_2water.addModelsAsFrames(two_waters)
    # view_2water.setStyle({'model': -1}, {style: {'color': 'spectrum'}})
    #view_2water.zoomTo()
    #view_2water.show()

def plot_morse(x, y):
    plt.ylim(-0.1, 2.1)
    plt.ylabel("E")
    plt.xlabel("$R-R_{eq}$")
    plt.plot(x, y)
    plt.show()

def make_animation(f, xs, qs, ylim=(-0.1,2.0)):
    fig, ax = plt.subplots()
    ax.grid()
    plt.ylim(ylim[0], ylim[1])
    plt.close()
    newtonAnimation = NewtonAnimation(f, ax, (-1.0, 5.0))
    return make_and_return_animation(fig, xs, qs, newtonAnimation)
